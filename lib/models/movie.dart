class Movie {
  String id;
  String title;
  String director;
  String summary;
  String genres;

  Movie({
    required this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.genres,
  });

  Map toMap() => {
        'id': id,
        'title': title,
        'director': director,
        'summary': summary,
        'genres': genres,
      };

  Movie.fromMap(Map map)
      : id = map['id'],
        title = map['title'],
        director = map['director'],
        summary = map['summary'],
        genres = map['genres'];
}
