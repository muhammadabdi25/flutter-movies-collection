import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_movies_collection_app/screens/movie_list_page.dart';
import 'package:flutter_movies_collection_app/screens/upsert_movie_page.dart';

part 'app_router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Page,Route')
class AppRouter extends _$AppRouter {

  @override
  List<AutoRoute> get routes => [
    AutoRoute(page: MovieListRoute.page, initial: true),
    AutoRoute(page: UpsertMovieRoute.page),
  ];
}