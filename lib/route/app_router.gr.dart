// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    MovieListRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const MovieListPage(),
      );
    },
    UpsertMovieRoute.name: (routeData) {
      final args = routeData.argsAs<UpsertMovieRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: UpsertMoviePage(
          key: args.key,
          id: args.id,
          movieTitleSuggestion: args.movieTitleSuggestion,
        ),
      );
    },
  };
}

/// generated route for
/// [MovieListPage]
class MovieListRoute extends PageRouteInfo<void> {
  const MovieListRoute({List<PageRouteInfo>? children})
      : super(
          MovieListRoute.name,
          initialChildren: children,
        );

  static const String name = 'MovieListRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [UpsertMoviePage]
class UpsertMovieRoute extends PageRouteInfo<UpsertMovieRouteArgs> {
  UpsertMovieRoute({
    Key? key,
    required String id,
    required String movieTitleSuggestion,
    List<PageRouteInfo>? children,
  }) : super(
          UpsertMovieRoute.name,
          args: UpsertMovieRouteArgs(
            key: key,
            id: id,
            movieTitleSuggestion: movieTitleSuggestion,
          ),
          initialChildren: children,
        );

  static const String name = 'UpsertMovieRoute';

  static const PageInfo<UpsertMovieRouteArgs> page =
      PageInfo<UpsertMovieRouteArgs>(name);
}

class UpsertMovieRouteArgs {
  const UpsertMovieRouteArgs({
    this.key,
    required this.id,
    required this.movieTitleSuggestion,
  });

  final Key? key;

  final String id;

  final String movieTitleSuggestion;

  @override
  String toString() {
    return 'UpsertMovieRouteArgs{key: $key, id: $id, movieTitleSuggestion: $movieTitleSuggestion}';
  }
}
