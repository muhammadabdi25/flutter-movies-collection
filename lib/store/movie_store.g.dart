// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieStore on _MovieStore, Store {
  late final _$movieListAtom =
      Atom(name: '_MovieStore.movieList', context: context);

  @override
  ObservableList<Movie> get movieList {
    _$movieListAtom.reportRead();
    return super.movieList;
  }

  @override
  set movieList(ObservableList<Movie> value) {
    _$movieListAtom.reportWrite(value, super.movieList, () {
      super.movieList = value;
    });
  }

  late final _$filteredMovieListAtom =
      Atom(name: '_MovieStore.filteredMovieList', context: context);

  @override
  ObservableList<Movie> get filteredMovieList {
    _$filteredMovieListAtom.reportRead();
    return super.filteredMovieList;
  }

  @override
  set filteredMovieList(ObservableList<Movie> value) {
    _$filteredMovieListAtom.reportWrite(value, super.filteredMovieList, () {
      super.filteredMovieList = value;
    });
  }

  late final _$queryAtom = Atom(name: '_MovieStore.query', context: context);

  @override
  String get query {
    _$queryAtom.reportRead();
    return super.query;
  }

  @override
  set query(String value) {
    _$queryAtom.reportWrite(value, super.query, () {
      super.query = value;
    });
  }

  late final _$_MovieStoreActionController =
      ActionController(name: '_MovieStore', context: context);

  @override
  void filterMovies() {
    final _$actionInfo = _$_MovieStoreActionController.startAction(
        name: '_MovieStore.filterMovies');
    try {
      return super.filterMovies();
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateQuery(String newQuery) {
    final _$actionInfo = _$_MovieStoreActionController.startAction(
        name: '_MovieStore.updateQuery');
    try {
      return super.updateQuery(newQuery);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addMovie(
      {required String title,
      required String director,
      required String summary,
      required String genres}) {
    final _$actionInfo =
        _$_MovieStoreActionController.startAction(name: '_MovieStore.addMovie');
    try {
      return super.addMovie(
          title: title, director: director, summary: summary, genres: genres);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateMovie(
      {required String id,
      required String title,
      required String director,
      required String summary,
      required String genres}) {
    final _$actionInfo = _$_MovieStoreActionController.startAction(
        name: '_MovieStore.updateMovie');
    try {
      return super.updateMovie(
          id: id,
          title: title,
          director: director,
          summary: summary,
          genres: genres);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void deleteMovie({required String id}) {
    final _$actionInfo = _$_MovieStoreActionController.startAction(
        name: '_MovieStore.deleteMovie');
    try {
      return super.deleteMovie(id: id);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
movieList: ${movieList},
filteredMovieList: ${filteredMovieList},
query: ${query}
    ''';
  }
}
