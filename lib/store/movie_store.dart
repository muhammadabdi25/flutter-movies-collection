import 'dart:math';

import 'package:flutter_movies_collection_app/models/movie.dart';
import 'package:mobx/mobx.dart';

part 'movie_store.g.dart';

class MovieStore = _MovieStore with _$MovieStore;

abstract class _MovieStore with Store {
  @observable
  ObservableList<Movie> movieList = ObservableList<Movie>.of([]);

  @observable
  ObservableList<Movie> filteredMovieList = ObservableList<Movie>.of([]);

  @observable
  String query = '';

  final _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  final Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  Movie? getMovieById(String id) {
    var filteredList = movieList.where((element) => element.id == id).toList();

    if (filteredList.isEmpty) return null;

    return filteredList.first;
  }

  @action
  void filterMovies() {
    filteredMovieList.clear();

    if (query.isEmpty) {
      filteredMovieList.addAll(movieList);
    } else {
      var filteredList = movieList
          .where((e) => e.title.toLowerCase().contains(query.toLowerCase()))
          .toList();

      filteredMovieList.addAll(filteredList);
    }
  }

  @action
  void updateQuery(String newQuery) {
    query = newQuery;
    filterMovies();
  }

  @action
  void addMovie({
    required String title,
    required String director,
    required String summary,
    required String genres,
  }) {
    movieList.add(Movie(
      id: getRandomString(10),
      title: title,
      director: director,
      summary: summary,
      genres: genres,
    ));
  }

  @action
  void updateMovie({
    required String id,
    required String title,
    required String director,
    required String summary,
    required String genres,
  }) {
    for (var movie in movieList) {
      if (movie.id == id) {
        movie.title = title;
        movie.director = director;
        movie.summary = summary;
        movie.genres = genres;
      }
    }
  }

  @action
  void deleteMovie({required String id}) {
    movieList.removeWhere((element) => element.id == id);
  }
}
