import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_movies_collection_app/main.dart';
import 'package:flutter_movies_collection_app/route/app_router.dart';
import 'package:flutter_movies_collection_app/store/movie_store.dart';

@RoutePage()
class MovieListPage extends StatefulWidget {
  const MovieListPage({super.key});

  @override
  State<MovieListPage> createState() => _MovieListPageState();
}

class _MovieListPageState extends State<MovieListPage> {
  final _movieStore = sl.get<MovieStore>();

  late TextEditingController _searchQueryController;

  @override
  void initState() {
    super.initState();
    _searchQueryController = TextEditingController();
    _movieStore.filterMovies();
  }

  @override
  void dispose() {
    _searchQueryController.dispose();
    super.dispose();
  }

  String _getFormattedGenres(String genres) {
    List<String> splitGenreNames = genres.split(',');

    return splitGenreNames.join(' / ');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movie Collection'),
      ),
      body: Observer(builder: (_) {
        return _movieStore.movieList.isEmpty
            ? Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text('There are no movies'),
                    TextButton(
                      onPressed: () => _navigateToUpsertMoviePage('', ''),
                      child: const Text('Add movie'),
                    ),
                  ],
                ),
              )
            : Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      controller: _searchQueryController,
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        hintText: 'Search by title',
                        labelText: 'Search',
                        suffixIcon: GestureDetector(
                            onTap: () {
                              _movieStore.updateQuery('');
                              _searchQueryController.clear();
                            },
                            child: const Icon(Icons.close_rounded)),
                      ),
                      onChanged: (text) {
                        _movieStore.updateQuery(text);
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  _movieStore.filteredMovieList.isEmpty
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('No movie with title ${_movieStore.query}'),
                            TextButton(
                              onPressed: () => _navigateToUpsertMoviePage(
                                '',
                                _movieStore.query,
                              ),
                              child: const Text('Add movie'),
                            ),
                          ],
                        )
                      : Expanded(
                          child: ListView.builder(
                            itemCount: _movieStore.filteredMovieList.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.only(
                                  left: 8.0,
                                  right: 8.0,
                                  bottom: 8.0,
                                ),
                                child: GestureDetector(
                                  onTap: () {
                                    _openMovieDetail(_movieStore
                                        .filteredMovieList[index].id);
                                  },
                                  child: Card(
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: [
                                          Text(
                                            _movieStore
                                                .filteredMovieList[index].title,
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium
                                                ?.copyWith(
                                                  fontWeight: FontWeight.w700,
                                                ),
                                          ),
                                          Text(
                                            _movieStore.filteredMovieList[index]
                                                .director,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyMedium,
                                          ),
                                          const SizedBox(height: 15),
                                          Text(
                                            _getFormattedGenres(
                                              _movieStore
                                                  .filteredMovieList[index]
                                                  .genres,
                                            ),
                                            style: Theme.of(context)
                                                .textTheme
                                                .labelMedium,
                                            textAlign: TextAlign.end,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                ],
              );
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _navigateToUpsertMoviePage('', '');
        },
        tooltip: 'Add random movie',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void _openMovieDetail(String id) {
    _navigateToUpsertMoviePage(id, '');
  }

  void _navigateToUpsertMoviePage(String id, String title) {
    context.router
        .push(UpsertMovieRoute(id: id, movieTitleSuggestion: title))
        .then((value) => _movieStore.filterMovies());
  }
}
