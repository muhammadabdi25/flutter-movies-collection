import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_movies_collection_app/main.dart';
import 'package:flutter_movies_collection_app/models/genre.dart';
import 'package:flutter_movies_collection_app/models/movie.dart';
import 'package:flutter_movies_collection_app/store/movie_store.dart';

@RoutePage()
class UpsertMoviePage extends StatefulWidget {
  const UpsertMoviePage({
    super.key,
    required this.id,
    required this.movieTitleSuggestion,
  });

  final String id;
  final String movieTitleSuggestion;

  @override
  State<UpsertMoviePage> createState() => _UpsertMoviePageState();
}

class _UpsertMoviePageState extends State<UpsertMoviePage> {
  late TextEditingController _titleController;
  late TextEditingController _directorController;
  late TextEditingController _summaryController;

  final _movieStore = sl.get<MovieStore>();

  List<Genre> allMovieGenres = <Genre>[
    Genre(name: 'Drama', isSelected: false),
    Genre(name: 'Action', isSelected: false),
    Genre(name: 'Animation', isSelected: false),
    Genre(name: 'Sci-Fi', isSelected: false),
    Genre(name: 'Horror', isSelected: false),
  ];

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController();
    _directorController = TextEditingController();
    _summaryController = TextEditingController();

    if (widget.movieTitleSuggestion.isNotEmpty) {
      _titleController.text = widget.movieTitleSuggestion;
    }

    if (widget.id.isEmpty) return;

    Movie? movie = _movieStore.getMovieById(widget.id);
    if (movie != null) _populateMovieData(movie);
  }

  @override
  void dispose() {
    _titleController.dispose();
    _directorController.dispose();
    _summaryController.dispose();
    super.dispose();
  }

  void _populateMovieData(Movie movie) {
    _titleController.text = movie.title;
    _directorController.text = movie.director;
    _summaryController.text = movie.summary;

    List<String> splitGenreNames = movie.genres.split(',');
    for (var genreName in splitGenreNames) {
      _selectGenre(genreName);
    }
  }

  void saveOrUpdateMovie() {
    if (_isEditMode()) {
      _movieStore.updateMovie(
        id: widget.id,
        title: _getMovieTitle(),
        director: _getMovieDirector(),
        summary: _getMovieSummary(),
        genres: _getMovieGenres(),
      );
    } else {
      _movieStore.addMovie(
        title: _getMovieTitle(),
        director: _getMovieDirector(),
        summary: _getMovieSummary(),
        genres: _getMovieGenres(),
      );
    }

    context.router.back();
  }

  String _getMovieTitle() {
    return _titleController.text;
  }

  String _getMovieDirector() {
    return _directorController.text;
  }

  String _getMovieSummary() {
    return _summaryController.text;
  }

  String _getMovieGenres() {
    var selectedGenreNames =
        allMovieGenres.where((e) => e.isSelected).map((e) => e.name).toList();
    String genreString = selectedGenreNames.join(',');
    return genreString;
  }

  Future<String?> deleteMovie() {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Confirmation'),
        content: Text('Are you sure to delete ${_getMovieTitle()}?'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text('Sure, delete'),
          ),
        ],
      ),
    );
  }

  bool _isEditMode() {
    return widget.id.isNotEmpty;
  }

  void _deleteMovie() {
    if (widget.id.isEmpty) return;
    _movieStore.deleteMovie(id: widget.id);

    context.router.back();
  }

  void _selectGenre(String name) {
    final newGenreList = allMovieGenres;
    for (var genre in newGenreList) {
      if (genre.name == name) {
        genre.isSelected = !genre.isSelected;
      }
    }

    setState(() {
      allMovieGenres = newGenreList;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text((_isEditMode()) ? 'Edit Movie' : 'Add Movie'),
        actions: [
          if (_isEditMode())
            GestureDetector(
              onTap: () => deleteMovie().then((result) {
                if (result == 'OK') _deleteMovie();
              }),
              child: const Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: Icon(Icons.delete_rounded),
              ),
            ),
          GestureDetector(
            onTap: () => saveOrUpdateMovie(),
            child: const Padding(
              padding: EdgeInsets.only(right: 15.0),
              child: Icon(Icons.save_rounded),
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: TextField(
              controller: _titleController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter movie title',
                labelText: 'Title',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: TextField(
              controller: _directorController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter movie director',
                labelText: 'Director',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: TextField(
              controller: _summaryController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter movie summary',
                labelText: 'Summary',
              ),
              minLines: 6,
              maxLines: 10,
              maxLength: 100,
            ),
          ),
          chipList(allMovieGenres),
        ],
      ),
    );
  }

  List<Widget> getChipList() {
    final genreChips = <Widget>[];
    for (var genre in allMovieGenres) {
      genreChips.add(_buildChip(genre.name, genre.isSelected));
    }

    return genreChips;
  }

  chipList(List<Genre> allMovieGenres) {
    return Wrap(
      spacing: 10.0,
      runSpacing: 16.0,
      children: getChipList(),
    );
  }

  Widget _buildChip(String label, bool isSelected) {
    return ActionChip(
      labelPadding: const EdgeInsets.all(2.0),
      avatar: isSelected
          ? const Icon(Icons.check_box_rounded)
          : const Icon(Icons.check_box_outline_blank_rounded),
      label: Text(label),
      elevation: isSelected ? 3.0 : 0.0,
      shadowColor: Colors.grey[60],
      padding: const EdgeInsets.all(8.0),
      onPressed: () {
        _selectGenre(label);
      },
    );
  }
}
