import 'package:flutter/material.dart';
import 'package:flutter_movies_collection_app/route/app_router.dart';
import 'package:flutter_movies_collection_app/store/movie_store.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;

void main() {
  sl.registerSingleton<MovieStore>(MovieStore());

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final appRouter = AppRouter();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: appRouter.config(),
      title: 'Movie Collection',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
    );
  }
}
